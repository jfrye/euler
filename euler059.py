import string
import itertools as it

with open("./data/euler59_data.txt") as f:
    data = f.readlines()[0]

encrypted_file_numbers = [int(x) for x in data.split(sep=",")]

lo_alphabet = list(string.ascii_lowercase)
up_alphabet = list(string.ascii_uppercase)
possible_passwords = it.product(lo_alphabet, repeat=3)

# for letter in lo_alphabet:
#     xorified = encrypted_file_numbers[0] ^ ord(letter)
#     print(xorified, chr(xorified))
#     # print(encrypted_file_numbers[0] ^ ord(letter), chr(encrypted_file_numbers[0] ^ ord(letter)))

# print(encrypted_file_numbers[0])
password = ['g', 'o', 'd']
password_ints = [ord(c) for c in password]

xor_converted = [encrypted_file_numbers[c] ^ password_ints[c % 3]
                 for c in range(len(encrypted_file_numbers))]

print(sum(xor_converted))
