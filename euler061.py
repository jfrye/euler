import sys
import itertools as it

def polygonal_number(sides, n):
    if sides == 3:
        return n*(n + 1)//2
    elif sides == 4:
        return n*n
    elif sides == 5:
        return n*(3*n - 1)//2
    elif sides == 6:
        return n*(2*n - 1)
    elif sides == 7:
        return n*(5*n - 3)//2
    elif sides == 8:
        return n*(3*n - 2)
    else:
        sys.exit(0)

polys = 6
poly_array = [[] for i in range(polys)]
intersects = [None] * polys
products   = [None] * polys

for sides in range(3, 3 + polys):
    i = 1
    x = polygonal_number(sides, i)
    while len(str(x)) < 5:
        if len(str(x)) == 4:
            poly_array[sides - 3].append(x)

        i += 1
        x = polygonal_number(sides, i)

poly_perms = list(it.permutations(poly_array))
for arr in poly_perms:
    pruned = list(arr)
    while sum(map(len, pruned)) > polys:
        lefts  = [[str(num)[:2] for num in side] for side in pruned]
        rights = [[str(num)[2:] for num in side] for side in pruned]

        for s in range(polys):
            intersects[s] = list(set(rights[s]) & set(lefts[(s + 1) % polys]))

        for s in range(polys): #
            products[(s + 1) % polys] = [int(x + y) for x in intersects[s % polys] for y in intersects[(s + 1) % polys]]
            pruned[(s + 1) % polys] = list(set(products[(s + 1) % polys]) & set(pruned[(s + 1) % polys]))

    if all(pruned):
        print(pruned)
        print(sum(x[0] for x in pruned))
        break

#for s in range(6):




#or s in range(6):
    
#print(poly_array[1], lefts[1], rights[1])

#print(test_prod)
#print(poly_array[1])



#for l in intersection[0]:
#    for r in intersection[0 + 1]:
#        if int(l + r) in poly_array[0 + 1]:
#            

    #print([[i for i, x in enumerate(rights[sides]) if x == y] for y in intersection])
