import src.integers as integ


total = 0
for number in range(2, 10000):
    prop_divs = integ.proper_divisors(number)
    prop_sum = sum(prop_divs)

    sum_prop_divs = integ.proper_divisors(prop_sum)

    if number != prop_sum and sum(sum_prop_divs) == number:
        print(number,':',prop_sum)
        total += number

print(total)
