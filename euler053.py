from math import factorial

def choose(n, r):
    return factorial(n) // (factorial(r)*factorial(n-r))
    
count = 0
for number in range(23, 101):
    for rumber in range(1, number + 1):
        if choose(number, rumber) > 1000000:
            count += 1

print(count)
    
