import sympy as sym
import math as m

n = 7
test = True

while test:
    n += 2
    if not sym.isprime(n):
        prime_list = sym.sieve.primerange(1, n)
        int_test = False
        for p in prime_list:
            test_num = m.sqrt(1/2 * (n - p))
            if test_num - int(test_num) == 0:
                int_test = True
                break
        
        if not int_test:
            test = False

print(n)
