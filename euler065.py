import fractions as fr

fraction_sequence = []

k = 1
for i in range(1, 10000):
    if i%3 == 2:
        fraction_sequence.append(2*k)
        k += 1
    else:
        fraction_sequence.append(1)

def e_continued_fraction(iterations):
    frac = fr.Fraction(0)
    for const in reversed(fraction_sequence[:iterations]):
        frac = 1 / (const + frac)

    return frac + 2


convergent_100 = e_continued_fraction(99).numerator

digit_list = list(str(convergent_100))
digit_list = list(map(int, digit_list))
print(sum(digit_list))

