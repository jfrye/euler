import sympy as sym

def cycles(number):
    string = str(number)

    return list(set([int(string[x:] + string[:x]) for x in range(len(string))]))

limit = 1000000
prime_list = list(sym.sieve.primerange(2, limit))

circular_count = 0
index = 0
while prime_list:
    cyc_list = cycles(prime_list[0])
    if all(map(sym.isprime, cyc_list)):
        circular_count += len(cyc_list)
        for item in cyc_list:
            prime_list.remove(item)

    else:
        prime_list.remove(prime_list[0])

print("The number of circular primes below", str(limit), "is", str(circular_count))
