import src.integers as integ

abundant_numbers = []

# Adds abundant numbers to list
for x in range(12, 28124):
    if integ.is_abundant(x):
        abundant_numbers.append(x)

abundant_sums = set(integ.list_of_pairwise_sums(abundant_numbers))

capped_abundant_sums = set([x for x in abundant_sums if x < 28124])

integer_range = set([x for x in range(1, 28124)])

abundantless = integer_range - capped_abundant_sums

print(sum(abundantless))
