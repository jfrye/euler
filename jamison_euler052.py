
def test(number):
    max_multiplier = 6
    sets = []
    for mutiplier in range(1, max_multiplier + 1):
        sets.append(list(str(number*mutiplier)))

    for i in range(1, max_multiplier):
        if set(sets[0]) != set(sets[i]):
            return False
        
    return True

num = 1
while True:
    if test(num):
        print(num)
        break
    num += 1
