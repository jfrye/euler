
def digit_sum(number):
    number = list(str(number))
    ls = list(map(int, number))
    return sum(ls)

maximum = 0
for a in range(1, 100):
    for b in range(1, 100):
        maximum = max(maximum, digit_sum(a**b))

print(maximum)
