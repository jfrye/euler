import fractions as fr

def root_two_continued_frac(iterations):
    frac = fr.Fraction(0)
    for i in range(iterations):
        frac = 1 / (2 + frac)

    return frac + 1

count = 0
for x in range(1, 1001):
    fraction = root_two_continued_frac(x)
    print(x)
    if len(str(fraction.numerator)) > len(str(fraction.denominator)):
        count += 1

print(count)
