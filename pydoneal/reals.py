"""
Functions for analyzing real numbers.
"""


def long_divide(numerator, denominator, precision=16):
	"""
	Does long division by hand to arbitrary precision.

	Parameters
	----------
	numerator : int
	denominator : int
	precision : int
		Number of digits after the decimal point

	Returns
	-------
	str
	"""
	output = ''

	for loop in range(1,precision+2):
		quotient = numerator // denominator
		remainder = numerator % denominator

		output += str(quotient)
		
		if loop == 1:
			output += '.'
		
		numerator = remainder * 10

		if remainder == 0:
			break	# checks if divides evenly

	return output
