"""
The following are functions useful for file input/output.
"""

import os


def extract_numbers_from_file(f_name, f_dir, delim=' '):
    """
    Extract numbers from a file with rows of numbers into a list of lists.

    This will only work for files containing integers for now!

    Parameters
    ----------
    f_name : str
        Name of input file.
    f_dir : str
        Directory containing the input file.
    delim : str
        Delimiter used in the input file to separate columns of numbers.

    Returns
    -------
    list of list of int
        List of lists of the integers from the input file.
    """
    os.chdir(f_dir)

    with open(f_name, 'r') as f:
        lines = f.readlines()

    for x in range(0, len(lines)):
        lines[x] = lines[x][0:lines[x].find('\n')]
        lines[x] = lines[x].split(delim)

        for y in range(0, len(lines[x])):
            lines[x][y] = int(lines[x][y])

    return lines
