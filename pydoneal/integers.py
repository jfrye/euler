"""
The following functions are useful for integer analysis and manipulation
"""

import sympy

from . import list_manip


def digits_to_list(integer):
    """
    Return the digits of the input integer as a list.

    Parameters
    ----------
    integer : int
        Integers whose digits will be extracted.

    Returns
    -------
    list of int
        List of digits of the input integer
    """
    arr = []

    while integer > 0:
        arr.append(int(integer % 10))
        integer //= 10

    return arr


def is_palindrome(integer):
    """
    Return True if the number is a palindrome.

    Parameters
    ----------
    integer : int

    Returns
    -------
    bool
    """
    reverse = int(str(integer)[::-1])

    return reverse - integer == 0


def least_common_multiple(divisors=list(range(1, 21))):
    """
    Return the least common multiple of the input divisors.

    Parameters
    ----------
    divisors : list of int

    Returns
    -------
    int
    """
    # No matter the divisors, we at least need the max divisor as an LCM
    lcm = max(divisors)
    lcm_factors = sympy.factorint(lcm)

    for n in divisors:
        n_factors = sympy.factorint(n)
        for f in n_factors:
            # If this factor appears in the LCM ...
            if f in lcm_factors:
                factor_excess = lcm_factors[f] - n_factors[f]
                # ... check if there are enough of them
                if factor_excess < 0:
                    # If not, we multiply into LCM to account for this
                    lcm *= - factor_excess * f
                    lcm_factors[f] += - factor_excess

            # If this factor does not appear in the LCM, multiple into LCM
            else:
                lcm *= n_factors[f] * f
                lcm_factors[f] = n_factors[f]

    return lcm


def list_of_fibonacci(limit):
    """
    Return a list of the Fibonacci sequence numbers that are less than limit.

    Parameters
    ----------
    limit : int

    Returns
    -------
    list of int
    """
    # Returns list of fibonacci numbers below <limit>

    f = [1, 2]      # first two fibonacci numbers
    n = 1           # iterator

    while f[n] < limit:
        f.append(f[n] + f[n - 1])   # add the next fib. num. to end of list
        n += 1                      # iterate

    return f


def list_of_products(vector):
    # Returns a list of all possible products between elements of <vector>
    table = list_manip.truth_table(len(vector))

    for row_num in range(0, len(table)):
        for col_num in range(0, len(table[row_num])):
            # each column is multipled by the corresponding vector element
            table[row_num][col_num] *= vector[col_num]
            # if it was zero, make it one, so when we multiply rows we don't
            # get zero
            if not table[row_num][col_num]:
                table[row_num][col_num] += 1

    prod_vector = [1] * len(table)        # init. vector of ones

    n = 0
    for row in table:
        for col in row:
            prod_vector[n] *= col
        n += 1

    # get rid of the entries of vector and 1 from prod_vector
    prod_vector.remove(1)
    for num in vector:
        prod_vector.remove(num)

    return prod_vector


def sum_of_multiples(lim, div):
    # This function calculates the sum of multiples of <div> up to <lim> using
    # Gauss' trick.

    quo = lim // div
    s = div * quo * (quo + 1) // 2      # Gauss' trick

    return s


def times_table(digits):
    # Returns a multiplication table for every number with <digits> digits

    factors = list(range(10 ** (digits - 1), 10 ** digits))[::-1]
    times_tab = []
    row = []

    for f1 in factors:
        for f2 in factors:
            product = f1 * f2
            row.append(product)
        times_tab.append(row)
        row = []

    return times_tab


def proper_divisors(number):
    if number <= 1:
        raise ValueError("Proper divisors of numbers <= 1 not defined.")

    prime_factors = sympy.factorint(number)
    prime_factors_exploded = [1]
    for key in prime_factors:
        for it in range(prime_factors[key]):
            prime_factors_exploded.append(key)

    all_products = list_of_products(prime_factors_exploded)

    prop_div_list = [x for x in all_products if x < number]
    prop_div_list.append(1)

    return sorted(list(set(prop_div_list)))


def is_abundant(number):
    # Return whether or not the number is 'abundant'; that is, the sum of its
    # proper divisors is greater than itself.
    return sum(proper_divisors(number)) > number


def list_of_pairwise_sums(vector):
    # Returns a list of pairwise sums between elements of <vector>

    sum_list = []

    for x in vector:
        for y in vector:
            sum_list.append(x + y)

    return list(set(sum_list))
