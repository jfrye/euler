

def cycle_length(numerator, denominator):

	remainder_list = []
	
	while True:
		remainder = numerator % denominator
		if remainder == 0:
			return 0
	
		if remainder in remainder_list:
			return len(remainder_list) - remainder_list.index(remainder)
		else:
			remainder_list.append(remainder)
	
		numerator = remainder * 10


longest_cycle = 0
longest_value = 0
for x in range(1,1000):
	cycle = cycle_length(1, x)
	if cycle > longest_cycle:
		longest_cycle = cycle
		longest_value = x

print(longest_value, '- cycle length =', longest_cycle)

