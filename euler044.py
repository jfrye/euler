import math
import sys


def pentagonal(n):
    return n*(3*n - 1) // 2

def is_pentagonal(n):
    tmp = (math.sqrt(24*n + 1) + 1) / 6
    decimal = tmp - int(tmp)
    if decimal > 0:
        return False
    return True
        

pent_nums = [5, 1]

x = 3
while True:
    pent = pentagonal(x)
    for value in pent_nums:
        if is_pentagonal(pent + value) and is_pentagonal(pent - value):
            print(pent, value, ':', pent - value)
            sys.exit(0)
    pent_nums.insert(0, pent)

    x += 1


    
