src package
===========

Submodules
----------

src.data_manip module
---------------------

.. automodule:: src.data_manip
    :members:
    :undoc-members:
    :show-inheritance:

src.integers module
-------------------

.. automodule:: src.integers
    :members:
    :undoc-members:
    :show-inheritance:

src.list_manip module
---------------------

.. automodule:: src.list_manip
    :members:
    :undoc-members:
    :show-inheritance:

src.primes module
-----------------

.. automodule:: src.primes
    :members:
    :undoc-members:
    :show-inheritance:

src.reals module
----------------

.. automodule:: src.reals
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:
